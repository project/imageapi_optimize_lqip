# Changelog

## ImageAPI Optimize LQIP 1.0.1, 2023-08-12

Changes since 1.0.0:

- Issue #3379448 - Swap image processing for API call
- Issue #3372518 - Fix the issues reported by phpcs
- Issue #3357214 - Integrate with the responsive image module.
- Housekeeping tasks.

## ImageAPI Optmize LQIP 1.0.0, 2023-05-08

- Initial build using GD.
